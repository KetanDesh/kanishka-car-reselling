@extends('layouts.app')

@section('content')
	<div class="container">

		@if(!empty($cars))
  		<div class="row row-cols-3 g-3">
  			@foreach($cars as $key=>$car)
			  <div class="col">
			    <div class="card" style="height:400px;">
			      <img src="{{$car['image']}}" class="card-img-top"
			        alt="{{$car['Car_Name']}}" />
			      <div class="card-body">
			        <h5 class="card-title"><b>Car Name -</b> {{$car['Car_Name']}}</h5>
			    	<h5 class="card-title"><b>Price -</b> ₹ {{$car['Price']}}</h5>
			    	<h5 class="card-title"><b>Car Type - </b>{{$car['Car_Type']}}</h5>

			      </div>
			    </div>
			  </div>
			@endforeach
		</div>
		@else
			<div>Records not found.</div>
		@endif
	</div>
@endsection