@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Car for Reselling</div>

                <div class="card-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                    <div>
                        <ul>
                            @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif
                   <form action="{{ route('car.save') }}" method="post" enctype="multipart/form-data">
                        @csrf
                      <div class="row">
                        <div class="col">
                            <label for="car name">Car Name</label>
                          <input type="text" class="form-control" placeholder="Car Name" name="Car_Name" value="">
                        </div>
                        <div class="col">
                            <label for="top speed">Car Top Speed</label>
                            <select class="form-select" name="Top_speed">
                                <option selected value="100 km/h">100 km/h</option>
                                <option  value="200 km/h">200 km/h</option>
                                <option  value="300 km/h">300 km/h </option>
                          </select>
                        </div>
                      </div><br>
                      <div class="row">
                        <div class="col">
                            <label for="price">Price</label>
                          <input type="number" class="form-control" placeholder="Price" name="Price" value="">
                        </div>
                        <div class="col">
                         <label for="images">Car Images (Multiple images)</label>
                          <input type="file" class="form-control" name="file[]" id="file" multiple>
                        </div>
                      </div><br>
                      <div class="row">
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
