<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('Car_Name')->nullable()->length(60);
            $table->enum('Top_Speed', ['100 km/h', '200 km/h', '300 km/h'])->default('100 km/h');
            $table->integer('Price')->nullable();
            $table->enum('Car_Type', ['Normal Car', 'Luxury Car', 'Sports Car'])->default('Normal Car');
            $table->text('Car_Images')->nullable();
            $table->timestamp('Created_At')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
