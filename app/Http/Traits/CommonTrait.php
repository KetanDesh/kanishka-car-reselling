<?php
namespace App\Http\Traits;
use Illuminate\Http\Request;

trait CommonTrait{
	public function indMoneyFormat($money){
		if (strpos($money, ',') !== false) return $money;
			$len = strlen ( $money );
			$m = '';
			$money = strrev ( $money );
							
		for($i = 0; $i < $len; $i ++) {
			if (($i == 3 || ($i > 3 && ($i - 1) % 2 == 0)) && $i != $len) {
				$m .= ',';
			}
			$m .= $money [$i];
		}
		return strrev ( $m );
	}
}
