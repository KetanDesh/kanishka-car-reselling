<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;
use App\Http\Traits\CommonTrait;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->cars = new cars();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    use CommonTrait; 
    public function index()
    {
        return view('home');
    }
    public function SaveCar(Request $request) {
        $car_speed_array = array("100 km/h"=>"Normal Car","200 km/h"=>"Luxury Car","300 km/h"=>"Sports Car");
        // Form validation
        $this->validate($request, [
            'Car_Name' => 'required',
            'Top_speed' => 'required',
            'Price' => 'required',
            'file' => 'required',
            'file.*'=>'required|mimes:png,jpg,jpeg'
         ]);

        try{
            $files = [];
            if ($request->file('file')){
                foreach($request->file('file') as $key => $file)
                {
                    $fileName = time().rand(1,99).'.'.$file->extension();  
                    $file->move(public_path('car_images'), $fileName);
                    $files[]= $fileName;
                }
            }
            
            $Car_Type = $car_speed_array[$request->input('Top_speed')];
            $save_data = array('Car_Name'=>$request->input('Car_Name'),"Car_Type"=>$Car_Type,"Top_speed"=>$request->input('Top_speed'),
                "Price"=>$request->input('Price'),"Car_images"=>implode(",",$files));
           
            //  Store data in database
            $save = $this->cars->SaveCar($save_data);
            return back()->with('success', 'Car Details Save Successfully');
        } catch(\Exception $e){
            return back()->with('failed', 'Something goes wrong!');
        }
    }
    public function CarView(Request $request){
        $cars = $this->cars->getCarsDetails();
        $cars_details = array();
        $host= request()->getSchemeAndHttpHost()."/car_images/";
        foreach($cars as $key=>$car){
            $images = explode(",",$car->Car_Images);
            $cars_details[] = array("Car_Name"=>$car->Car_Name,"Price"=>$this->indMoneyFormat($car->Price),"Car_Type"=>$car->Car_Type,
                "image"=>$host.$images[0]);
        }
        return view('car_views',['cars'=>$cars_details]);
    }
}
