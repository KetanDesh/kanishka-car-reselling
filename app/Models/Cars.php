<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Cars extends Model
{
    use HasFactory;
    public function SaveCar($save_data){
        $id = DB::table('cars')->insert($save_data);
        return $id;
    }
    public function getCarsDetails(){
        $cars = DB::table('cars')->orderby('id','DESC')->get();
        return $cars;
    }
}
